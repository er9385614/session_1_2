import '../../common/utils.dart';
import '../data/repository/supabase.dart';

class ForgotPasswordPresenter {
  bool isValid(String email){
    return checkEmail(email);
  }

  Future<void> pressSendCode(
      String email,
      Function(void) onResponse,
      Function(String) onError
      ) async {
    request(
        request: () async {
          await sendCode(email);
        },
        onResponse: onResponse,
        onError: onError
    );
  }
}