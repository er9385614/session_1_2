import 'package:ses_ion_1_2/auth/data/repository/supabase.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/utils.dart';

class HolderPresenter {

  late SharedPreferences sharedPreferences;

  HolderPresenter(){
    SharedPreferences.getInstance().then((value) => sharedPreferences = value);
  }

  Future<void> pressExitButton(
      Function() onResponse,
      Function(String) onError
      ) async {
    await request(
        request: signOut,
        onResponse: (_) async {
          await sharedPreferences.clear();
          onResponse();
        },
        onError: onError
    );
  }
}