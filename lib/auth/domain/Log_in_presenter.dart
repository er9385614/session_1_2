import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../common/utils.dart';
import '../data/models/model_auth.dart';
import '../data/repository/supabase.dart';

class SignInPresenter {

  final SharedPreferences sharedPreferences;

  SignInPresenter({required this.sharedPreferences});

  static Future<SignInPresenter> init() async {
    var sh = await SharedPreferences.getInstance();
    return SignInPresenter(sharedPreferences: sh);
  }

  Future<void> logoutIfAuth() async {
    if (supabase.auth.currentUser != null){
      await request(
          request: signOut,
          onResponse: (_){},
          onError: (_){}
      );
    }
  }

  bool isValid(
      String email,
      String password
      ){
    return email.isNotEmpty && password.isNotEmpty &&
        checkEmail(email);
  }

  void checkExistAuthData(
      Function(ModelAuth) onExist
      ){
    if (sharedPreferences.containsKey("email")){
      var model = ModelAuth(
          email: sharedPreferences.getString("email")!,
          password: sharedPreferences.getString("hashPassword")!
      );
      onExist(model);
    }
  }

  void useSavedAuthData(
      ModelAuth data,
      Function(AuthResponse) onResponse,
      Function(String) onError,
      ) {
    request(
        request: () async { return await logIn(data); },
        onResponse: onResponse,
        onError: onError
    );
  }

  void pressSignIn(
      String email,
      String hashPassword,
      bool isRememberMe,
      Function(AuthResponse) onResponse,
      Function(String) onError,
      ){
    ModelAuth modelAuth = ModelAuth(email: email, password: hashPassword);
    request(
        request: () async { return await logIn(modelAuth); },
        onResponse: (AuthResponse e) async {
          if (isRememberMe){
            await sharedPreferences.setString("email", email);
            await sharedPreferences.setString("hashPassword", hashPassword);
          }
          onResponse(e);
        },
        onError: onError
    );
  }
}