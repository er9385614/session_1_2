import 'package:supabase_flutter/supabase_flutter.dart';

import '../../common/utils.dart';
import '../data/repository/supabase.dart';

class SetNewPasswordPresenter {

  bool isValid(String password, String confirmPassword){
    return password == confirmPassword;
  }

  Future<void> pressConfirmNewPassword(
      String newPassword,
      Function(UserResponse) onResponse,
      Function(String) onError
      ) async {
    request(
        request: () async {
          var res = await updatePassword(newPassword);
          await signOut();
          return res;
        },
        onResponse: onResponse,
        onError: onError
    );
  }
}