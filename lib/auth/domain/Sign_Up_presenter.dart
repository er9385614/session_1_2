import 'package:flutter/cupertino.dart';
import 'package:ses_ion_1_2/auth/data/models/model_auth.dart';
import 'package:ses_ion_1_2/common/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/repository/supabase.dart';

class Sign_up_presenter {

  final BuildContext context;

  Sign_up_presenter({required this.context});

 bool isValid(
     String email,
     String password,
     String confirmPassword
     ){
   return email.isNotEmpty && password.isNotEmpty
       && confirmPassword == password && checkEmail(email);
 }

 void pressSignUp(
     String email,
     String password,
     Function(AuthResponse) onResponse
     ){
   ModelAuth modelAuth = ModelAuth(email: email, password: password);
   request(
       request: () async { return await signUp(modelAuth); },
       onResponse: onResponse,
       onError: (error){
         showErrorDialog(context, error);
       }
   );
 }
}