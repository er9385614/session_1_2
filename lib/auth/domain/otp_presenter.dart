import 'dart:async';

import 'package:supabase_flutter/supabase_flutter.dart';

import '../../common/utils.dart';
import '../data/repository/supabase.dart';

class OtpPresenter {

  var lostSecond = 60;

  void decrementSecond(){
    if (lostSecond != 0){
      lostSecond --;
    }
  }

  void startDelayDecrement(Function(int) callback){
    Timer(const Duration(seconds: 1), () {
      decrementSecond();
      callback(lostSecond);
      startDelayDecrement(callback);
    });
  }

  Future<void> resendCode(
      String email,
      Function() onResponse,
      Function(String) onError
      ) async {
  }

  Future<void> verifyCode(
      String email,
      String code,
      Function(AuthResponse) onResponse,
      Function(String) onError
      ) async {
    request(
        request: () async {
          return await verifyOtpCode(email, code);
        },
        onResponse: onResponse,
        onError: onError
    );
  }
}