import 'package:flutter/material.dart';

import '../../../common/widgets/CustomTextField.dart';
import 'Log_In.dart';

class Forgot_Password extends StatefulWidget {
  Forgot_Password({super.key});

  @override
  State<Forgot_Password> createState() => _Forgot_PasswordState();
}

class _Forgot_PasswordState extends State<Forgot_Password> {
  var emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, bottom: 38, top: 45),
        child: Column(
          children: [
            const SizedBox(height: 38),
            Row(children: [
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text("Восстановление пароля",
                    style: Theme.of(context).textTheme.labelLarge),
                Text("Введите свою почту",
                    style: Theme.of(context).textTheme.labelMedium),
              ])
            ]),
            CustomTextField(
              label: "Почта",
              hint: "************@mail.com",
              controller: emailController,
              // onChange: isValid
            ),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                        onPressed: () {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(builder: (_) => Log_In()),
                              (route) => false);
                        },
                        child: Text("Отправить код",
                            style: Theme.of(context).textTheme.labelSmall))),
              ],
            )),
            const SizedBox(height: 14),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) => Log_In()));
              },
              child: RichText(
                  text: TextSpan(children: [
                TextSpan(
                    text: "Я вспомнил свой пароль!",
                    style: Theme.of(context).textTheme.labelMedium),
                TextSpan(
                    text: "Войти",
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                        color: const Color.fromARGB(255, 106, 139, 249))),
              ])),
            ),
            const SizedBox(height: 34)
          ],
        ),
      ),
    );
  }
}
