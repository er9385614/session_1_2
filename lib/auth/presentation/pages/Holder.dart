import 'dart:io';

import 'package:flutter/material.dart';
import '../../../common/utils.dart';
import '../../domain/Holder_presenter.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}

class _HolderState extends State<Holder> {

  var presenter = HolderPresenter();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 22),
          child: SizedBox(
            width: double.infinity,
            child: FilledButton(
              onPressed: () {
                presenter.pressExitButton(
                        () {
                      exit(0);
                    },
                        (error) {
                      showErrorDialog(context, error);
                    }
                );
              },
              child: const Text("ВЫХОД"),
            ),
          ),
        ),
      ),
    );
  }
}