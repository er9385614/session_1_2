import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ses_ion_1_2/auth/presentation/pages/Forgot_password.dart';
import 'package:ses_ion_1_2/auth/presentation/pages/Holder.dart';
import 'package:ses_ion_1_2/auth/presentation/pages/Sign_Up.dart';

import '../../../common/widgets/CustomTextField.dart';

class Log_In extends StatefulWidget {
  @override
  State<Log_In> createState() => _Log_InState();
}

class _Log_InState extends State<Log_In> {

  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  bool rememberPassword = false;


  void onChange(_) {
    setState(() {
      emailController.text.isNotEmpty && passwordController.text.isNotEmpty;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
            padding: EdgeInsets.only(left: 24, right: 24, bottom: 38, top: 45),
            child: Column(children: [
              const SizedBox(height: 38),
              Row(children: [
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text("Создать аккаунт",
                      style: Theme.of(context).textTheme.labelLarge),
                  const SizedBox(height: 8),
                  Text(
                    "Завершите регистрацию чтобы начать",
                    style: Theme.of(context).textTheme.labelMedium,),
                ]),
              ]),
              CustomTextField(
                  label: "Почта",
                  hint: "************@mail.com",
                  controller: emailController,
                  onChange: onChange),
              CustomTextField(
                  label: "Пароль",
                  hint: "***********",
                  controller: passwordController,
                  enableObscure: true,
                  onChange: onChange),
              SizedBox(height: 18,),
              Row(
                children: [
                  SizedBox.square(
                    dimension: 22,
                    child: Transform.scale(
                      scale: 1.2,
                      child: Checkbox(
                          value: rememberPassword,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                    
                          onChanged: (newValue){
                        setState(() {
                          rememberPassword = newValue!;
                        });
                      }
                      ),
                    ),
                  ),
                  Expanded(
                      child: Text(
                          "Запомнить меня",
                          style: Theme.of(context).textTheme.labelMedium
                          )
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (_) => Forgot_Password()));
                    },
                    child: Text("Забыли пароль?",
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                        color: Color.fromARGB(255, 106, 139, 249)),),
                  )
                ],
              ),
              Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                          width: double.infinity,
                          child: FilledButton(
                              onPressed: (){
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(builder: (_) => Holder()),
                                        (route) => false);
                              },
                              child: Text("Войти",
                                  style: Theme.of(context).textTheme.labelSmall))
                      ),
                    ],)
              ),
              const SizedBox(height: 14),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (_) => Sign_up())
                  );
                },
                child: RichText(text: TextSpan(
                    children: [
                      TextSpan(
                          text: "У меня нет аккаунта! ",
                          style: Theme.of(context).textTheme.labelMedium
                      ),
                      TextSpan(
                          text: "Создать",
                          style: Theme.of(context).textTheme.labelMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249))                     ),
                    ]
                )),
              ),
              const SizedBox(height: 34)
            ])
        )
    );
  }
}
