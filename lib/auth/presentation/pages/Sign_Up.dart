import 'dart:core';
import 'package:flutter/material.dart';
import 'package:ses_ion_1_2/auth/presentation/pages/Log_In.dart';
import 'package:ses_ion_1_2/common/widgets/CustomTextField.dart';

class Sign_up extends StatefulWidget {
  const Sign_up({super.key});

  @override
  State<Sign_up> createState() => _Sign_upState();
}

class _Sign_upState extends State<Sign_up> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  bool isObscurePassword = true;
  bool isObscureConfirmPassword = true;
  bool isChecked = false;
  bool enableButton = false;

  void onChange(_) {
    setState(() {
      emailController.text.isNotEmpty && passwordController.text.isNotEmpty &&
          confirmPasswordController.text.isNotEmpty && passwordController.text
          == confirmPasswordController.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 24, right: 24, bottom: 38, top: 45),
        child: Column(
          children: [
            const SizedBox(height: 38),
            Row(
              children: [
                Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Создать аккаунт",
                        style: Theme
                            .of(context)
                            .textTheme
                            .labelLarge),
                    const SizedBox(height: 8),
                    Text("Завершите регистрацию чтобы начать",
                      style: Theme
                          .of(context)
                          .textTheme
                          .labelMedium,),
                  ]
              ),
            ]),
            CustomTextField(
                label: "Почта",
                hint: "************@mail.com",
                controller: emailController,
                onChange: onChange),
            CustomTextField(
                label: "Пароль",
                hint: "***********",
                controller: passwordController,
                enableObscure: true,
                onChange: onChange),
            CustomTextField(
              label: "Повторите пароль",
              hint: "**********",
              controller: confirmPasswordController,
              enableObscure: true,
              onChange: onChange),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                        onPressed: (){
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(builder: (_) => Log_In()),
                                  (route) => false);
                        },
                        child: Text("Зарегистрироваться",
                        style: Theme.of(context).textTheme.labelSmall))
                    ),
                ],)
            ),
            const SizedBox(height: 14),
            GestureDetector(
              onTap: (){
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) => Log_In())
                );
              },
              child: RichText(text: TextSpan(
                  children: [
                    TextSpan(
                        text: "У меня уже есть аккаунт! ",
                        style: Theme.of(context).textTheme.labelMedium
                    ),
                    TextSpan(
                        text: "Войти",
                        style: Theme.of(context).textTheme.labelMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249))                     ),
                  ]
              )),
            ),
            const SizedBox(height: 34)
          ],
        ),
      ),
    );
  }
}