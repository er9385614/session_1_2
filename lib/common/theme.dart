import 'package:flutter/material.dart';

var lightTheme = ThemeData(
  textTheme: const TextTheme(
    labelLarge: TextStyle(
      color: Color.fromARGB(255, 58, 58, 58),
      fontSize: 24,
      fontWeight: FontWeight.w700
    ),
    labelMedium: TextStyle(
      color: Color.fromARGB(255, 129, 129, 129),
      fontSize: 14,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto"
    ),
    labelSmall: TextStyle(
      color: Color.fromARGB(255, 255, 255, 255),
      fontSize: 16,
      fontWeight: FontWeight.w700,
      fontFamily: "Roboto"
    )
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: Color.fromARGB(255, 106, 139, 249),
      disabledBackgroundColor: Color.fromARGB(255, 167, 167, 167),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4))
    )
  )
);