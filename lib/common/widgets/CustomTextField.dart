import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final Function(String)? onChange;

  const CustomTextField(
      {super.key,
      required this.label,
      required this.hint,
      required this.controller,
      this.enableObscure = false,
      this.onChange});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  bool isObscure = true;

  @override
  Widget build(BuildContext context) {


    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 28,
        ),
        Text(
          widget.label,
          style: Theme.of(context).textTheme.labelMedium,
        ),
        const SizedBox(
          height: 8,
        ),
        SizedBox(
          width: double.infinity,
          height: 44,
          child: TextField(
            controller: widget.controller,
            obscureText: (widget.enableObscure) ? isObscure : false,
            obscuringCharacter: "*",
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4),
                  borderSide: const BorderSide(
                      width: 1, color: Color.fromARGB(255, 129, 129, 129)),
                ),
                suffixIcon: (widget.enableObscure)
                    ? GestureDetector(
                        onTap: () {
                          setState(() {
                            isObscure = !isObscure;
                          });
                        },
                        child: Image.asset("assets/eye-slash.png"),
                      )
                    : null,
                contentPadding:
                    const EdgeInsets.only(top: 14, bottom: 14, left: 10),
                hintText: widget.hint,
                hintStyle: Theme.of(context).textTheme.labelMedium?.copyWith(
                    color: const Color.fromARGB(255, 207, 207, 207))),
            onChanged: widget.onChange,
          ),
        ),
      ],
    );
  }
}
